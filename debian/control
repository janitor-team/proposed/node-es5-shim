Source: node-es5-shim
Section: javascript
Priority: optional
Maintainer: Debian Javascript Maintainers <pkg-javascript-devel@lists.alioth.debian.org>
Uploaders: Julien Puydt <jpuydt@debian.org>
Build-Depends: debhelper-compat (= 13), nodejs, uglifyjs
Standards-Version: 4.6.0
Rules-Requires-Root: no
Homepage: https://github.com/es-shims/es5-shim/
Vcs-Git: https://salsa.debian.org/js-team/node-es5-shim.git
Vcs-Browser: https://salsa.debian.org/js-team/node-es5-shim

Package: node-es5-shim
Architecture: all
Multi-Arch: foreign
Depends: nodejs, ${misc:Depends}, libjs-es5-shim (= ${binary:Version})
Description: ECMAScript 5 compatibility shims for old JavaScript engines (Node.js)
 The es5-shim library monkey-patches a JavaScript context to contain
 all ECMAScript 5 methods that can be faithfully emulated with a
 legacy JavaScript engine.
 .
 The es5-sham part tries to complete the picture as best as possible, but
 for those methods the result is not as close and mostly intends to avoid
 runtime errors : in many cases it might just silently fail.
 .
 Node.js is an event-based server-side JavaScript engine.

Package: libjs-es5-shim
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: ECMAScript 5 compatibility shims for old JavaScript engines (library)
 The es5-shim library monkey-patches a JavaScript context to contain
 all ECMAScript 5 methods that can be faithfully emulated with a
 legacy JavaScript engine.
 .
 The es5-sham part tries to complete the picture as best as possible, but
 for those methods the result is not as close and mostly intends to avoid
 runtime errors : in many cases it might just silently fail.
